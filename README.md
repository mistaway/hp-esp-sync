# HP - ESP - Real Time Daya Sync Service

This service will connect to the mqtt broker and subscribe to specific topics. The messages will be used for predictive modeling.

## Requirements

Docker and Docker Compose are required to run this container.

## Usage

Copy the source from the repo to the host that has docker and docker compose installed.

Create a file named `.env` in the same directory as `Dockerfile` containing the environment variables required to run the container. The default config for this file will be shared separately. 

Of importance is the environment variable `CHANNELLIST` that shall contain a comma separated list of all data variables desired to be studied on each device. This environment variable can be changed at any time to modify the service.

Then you just need to run

```shell
docker-compose  up -d --build
```

If the variable list requires updating, after changing and saving the modified `.env` file just stop and start the service.

```shell
docker-compose  down
docker-compose  up -d --build
```
## Responsibilities
Enfinite Technologies will be responsible for modifying app.py as necessary for ingesting messages into their infrastructure.

Enfinite Technologies shall not modify the subscription to p2121 mqtt broker in app.py (outside of modifying the `CHANNELLIST` environment variable in `.env`).

Enfinite Technologies is repsonsible for the uptime of the service once launched on their infrastructure. 