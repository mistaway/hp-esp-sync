# ESP and Motor Data:

|    | variable                | channel                                                                                | plctag                |
|----|-------------------------|----------------------------------------------------------------------------------------|-----------------------|
| 1  | motor current           | vfdcurrent                                                                             | vfd_outcurrent        |
| 2  | motor frequency         | vfdfrequency                                                                           | vfd_speedfdbk         |
| 3  | motor temperature       | intaketemperature                                                                      | val_IntakeTemperature |
| 4  | pump intake pressure    | intakepressure                                                                         | val_intakepressure    |
| 5  | pump discharge pressure | tubingpressure                                                                         | val_TubingPressure    |
| 6  | pump intake temperature | see #3                                                                                 | see #3                |
| 7  | vibration (x,y,z axis)  | HP does not pull                                                                       | HP does not pull      |
| 8  | current leakage         | HP does not pull                                                                       | HP does not pull      |
| 9  | output amps             | see #1                                                                                 | see #1                |
| 10 | setpoint information    | - flowsetpoint - fluidlevelsetpoint - manualfrequencysetpoint - tubingpressuresetpoint |                       |

# Production Data:
|   | variable        | channel        | plctag                         |
|---|-----------------|----------------|--------------------------------|
| 1 | water rate      | flowrate       | val_flowmeter                  |
| 2 | oil rate        | NA             | NA                             |
| 3 | gas rate        | NA             | NA                             |
| 4 | tubing pressure | tubingpressure | val_tubingpressure             |
| 5 | casing pressure | NA             | HP do not pull on a water well |

# Historical ESP Failure Timestamps
p21 provide alarm history, HP can assist with interpretation 

# ESP Design Configuration
p21 do not store this information in the cloud.
All of 2 will have to come from HP pump department.