#!python3
## mqtt demo code provided by Steve Cope at www.steves-internet-guide.com
## kevin@mistaway.com , sept 2020 , for hp-esp project
import os

USERNAME = str(os.environ["USERNAME"])
PASSWORD = str(os.environ["PASSWORD"])
BROKER = str(os.environ["BROKER"])
DEBUG = str(os.environ["DEBUG"])
CHANNELLIST = str(os.environ["CHANNELLIST"])
CHANNELLIST = CHANNELLIST.split(",")

import paho.mqtt.client as mqtt  # import the client
import time
import sys
import json


def initialise_client(cname):
    print("initializing client")

    # callback assignments below
    client = mqtt.Client(cname)
    client.cname = cname
    if DEBUG == "True":
        client.on_log = on_log
    client.on_connect = on_connect
    client.on_message = on_message
    client.will_set("p2121/services/" + cname + "/status", "last will", 2)
    # client.on_subscribe=on_subscribe

    # client custom flags below
    # client.topic_ack=[]
    client.run_flag = False
    # client.running_loop=False
    # client.subscribe_flag=False
    # client.disconnect_time=0.0
    client.tx = 0
    client.rx = 0
    client.bad_connection_flag = False
    client.connected_flag = False
    client.disconnect_flag = False
    client.ver = "0.0.9"

    return client


def on_log(client, userdata, level, buf):
    print("log: ", buf)


def on_disconnect(client, userdata, rc):
    print("disconnecting reason  " + str(rc))
    client.publish(
        "p2121/services/" + client.cname + "/status", "quit " + str(rc)
    )  # publish
    client.connected_flag = False
    client.disconnect_flag = True


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        client.connected_flag = True
        for channel in CHANNELLIST:
            client.subscribe("meshify/db/194/_/advvfdipp/+/" + channel)
        client.publish("p2121/services/" + client.cname + "/status", "start " + str(rc))
    else:
        print("Bad connection Returned code=", rc)
        client.bad_connection_flag = True


def on_message(client, userdata, message):
    # devices do not send timestamp so we make one here
    timestamp = time.time()

    # mac uniquely identifies the device reporting data
    mac = message.topic.split("/")[5]

    # channel is the datatype being reported
    channel = message.topic.split("/")[6]

    payload = message.payload.decode("utf-8").replace("'", '"')
    client.rx += 1
    data = json.loads(payload)
    # data is the value of the channel being reported
    data = data[0]["value"]

    # do your stuff here
    espParse(timestamp, mac, channel, data)


def espParse(timestamp, mac, channel, data):
    print(timestamp, mac, channel, data)


# Begin Main App

broker_address = BROKER

clientname = "hp-esp-" + str(int(time.time()))
print("creating new instance " + clientname)
client = initialise_client(clientname)

print("connecting to broker")
client.username_pw_set(username=USERNAME, password=PASSWORD)
client.connect(broker_address, keepalive=120)  # connect to broker

print("Start Main Loop")
client.loop_start()
while not client.connected_flag and not client.bad_connection_flag:
    print(client.connected_flag)
    time.sleep(0.1)
if client.bad_connection_flag:
    client.loop_stop()
    sys.exit()

client.run_flag = True

while client.run_flag:
    print("in main loop")
    if client.connected_flag:  # publish if have a connection
        client.publish("p2121/services/" + clientname + "/tx", str(client.tx))
        client.publish("p2121/services/" + clientname + "/rx", str(client.rx))
        client.publish("p2121/services/" + clientname + "/ver", str(client.ver))
        time.sleep(60 * 60)